(ns rotation.core
  (:gen-class)
  (:require [clojure.string :as string]))

(def matrix-string
  (string/split-lines
   "LTTTTR
    L****R
    L****R
    L****R
    L****R
    LBBBBR"))

(def matrix (map #(->> %1
                       string/trim
                       seq
                       (map str)) matrix-string))

(defn rotate
  ([matrix]
   (apply map #(into [] %&) (reverse matrix)))
  ([direction times matrix]
   (if (> times 0)
     (let [matrix-prime (case direction
                          :left  (-> matrix reverse rotate reverse)
                          :right (rotate matrix))]
       (rotate direction (dec times) matrix-prime))
     matrix)))

(defn pm [matrix]
  (let [rows (map #(reduce str %) matrix)]
    (print (string/join "\n" rows))))
